<h1>9sp.in</h1>
<h2>Url shortener that does not discriminate against fat urls.</h2>
No more errors or failures when trying to shorten and share your extremely long URLs. In today’s world of web applications and PHP/JavaScript query strings, URLs can grown to be very long. When tools like Bitly or TinyURL produce non-descriptive errors, 9sp.in produces short, usable URLs.
<br><br>
Bitly and TinyURL also share and sell your data for industry analysis, demographic profiling, and targeted ads. 9sp.in strives for quality, secure, and open code. This allows for a private URL shortening tool. Your data can stay your data.
<br><br>
Behind the scenes, 9sp.in is a serverless app running on top of AWS Simple Storage Service (S3). Using S3 as our file storage web service, allows us to achieve scalability, high availability, and low latency at minimal costs. Each URL shorten call invokes a Lambda function that puts a redirect object with minimum context size. That object or link can then be used and shared rather than a bulky and long URL.
<h2>Getting Started.</h2>
<ol>
 <li>Visit  <a href="http://www.9sp.in/" target="_blank">http://www.9sp.in/</a></li>
 <li>Enter desired URL to shorten</li>
 <li>Click ‘shorten’</li>
 <li>Use and share the new short 9sp.in URL provided</li>
</ol>
<h2>Deploying to a private AWS environment</h2>
The following outlines how to run and operate a private URL shortening service in your AWS environment using 9sp.in's code:
<ol>
 <li>Create IAM policy
  <ul><li>Policy generator
   <ol>
    <li>Effect: Allow</li>
    <li>AWS Service: AWS Lambda</li>
    <li>Actions: InvokeFunction</li>
    <li>ARN: arn:aws:lambda:*:*:function:MyUrlShortenFunction</li>
    <li>Add statement</li>
    <li>Effect: Allow</li>
    <li>AWS Service: Amazon S3</li>
    <li>Actions:
     <ul>
      <li>PutObject</li>
      <li>PutObjectAcl</li>
     </ul>
    <li>ARN: arn:aws:s3:::www.myurlshortensite.com/*</li></ol>
  <li>Policy Name: MyUrlShortenPolicy</li></ul>
 <li>Create new role
  <ul><li>Role name: MyUrlShortenRole</li>
  <li>AWS Service Role: AWS Lambda</li>
  <li>Attach Policy: MyUrlShortenPolicy</li></ul>
 <li>Create an IAM user
  <ul><li>User name: MyUrlShortenUser</li>
  <li>Access type: Programmatic access</li>
  <li>Attach existing policies directly
   <ol><li>Select MyUrlShortenPolicy</li></ol>
  <li>Create User</li>
  <li>Note Access key ID and Secret key ID</li></ul>
 <li>Create a lambda python function
  <ul><li>Name: MyUrlShortenFunction</li>
  <li>Runtime: Python 2.7</li>
  <li>Lambda function code:
   <ol><li><a href="https://gitlab.com/jason.fletcher/9sp.in/blob/master/lambda-function">lambda-fuction</a></li></ol>
  <li>Role: Choose an existing role</li>
  <li>Existing role: MyUrlShortenRole</li></ul>
 <li>Create an S3 bucket
  <ul><li>Bucket name: www.myurlshortensite.com</li>
  <li>Selection region, i.e. ‘US West Oregon’</li>
  <li>Set properties
   <ol><li>Update properties, Metadata
    <ul><li>Content type: text/html</li></ul></ol>
  <li>Set permissions
   <ol><li>Manage public permissions
    <ul><li>Everyone
     <ol><li>Objects: Read</li></ol></ul></ul>
 <li>Create a html file with the following content
  <ul><li><a href="https://gitlab.com/jason.fletcher/9sp.in/blob/master/index">index</a></li></ul>
 <li>Select created bucket and upload the following index file
  <ul><li>Upload index</li>
  <li>Set permissions
   <ol><li>Manage public permissions
    <ul><li>Everyone
     <ol><li>Objects: Read</li></ol></ul>
  <li>Update properties, Metadata
   <ol><li>Content type: text/html</li>
   <li>Save</li></ol></ul>
 <li>Test and create a shortened URL:
  <ul><li>Browse to site https://s3-us-west-2.amazonaws.com/www.myurlshortensite.com/index</li>
  <li>Enter a URL to shorten</li>
  <li>Click Shorten</li>
  <li>Click provided link</li>
</ol>
<h2>Questions or comments to jason@9sp.in</h2>